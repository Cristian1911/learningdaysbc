#!/bin/bash

while [[ $# -gt 0 ]]; do
  case $1 in
    -u|--update)
      UPDATE_SWITCH="yes"
      shift # past argument
      shift # past value
      ;;
    -cf|--credetials-file)
      CREDENTIALS_FILE_PATH="$2"
      shift # past argument
      shift # past value
      ;;
    -pf|--params-file)
      PARAMS_FILE_PATH="$2"
      shift # past argument
      shift # past value
      ;;
    -tf|--template-file)
      TEMPLATE_FILE_PATH="$2"
      shift # past argument
      shift # past value
      ;;
    -sn|--stack-name)
      STACK_NAME="$2"
      shift # past argument
      shift # past value
      ;;
    -*|--*)
      echo "Unknown option $1"
      exit 1
      ;;
  esac
done

PWD="file://$(pwd)"
# Set cli variables
CREDENTIALS_FILE_PATH=${CREDENTIALS_FILE_PATH:-"credentials"}
PARAMS_FILE_PATH="$PWD/${PARAMS_FILE_PATH:-"params.json"}"
TEMPLATE_FILE_PATH="$PWD/${TEMPLATE_FILE_PATH:-"template.yaml"}"
STACK_NAME=${STACK_NAME:-"ia-team-eiafanad-jccuadro"}
UPDATE_SWITCH=${UPDATE_SWITCH:-"no"}

# Now export all credentials aws variables to shell session
echo "Exporting aws credentials to shell session"
echo "credentials_file_path = $CREDENTIALS_FILE_PATH"

sleep 2

export $(grep -v '^#' $CREDENTIALS_FILE_PATH | xargs -0)

# Execute aws command for deploying stack
echo "params_file_path = $PARAMS_FILE_PATH"
echo "template_file_path = $TEMPLATE_FILE_PATH"
echo "stack_name = $STACK_NAME"

sleep 2

if [[ $UPDATE_SWITCH == "no" ]]; then
  echo "creating mode stack"
  aws cloudformation create-stack --stack-name $STACK_NAME --template-body $TEMPLATE_FILE_PATH --parameters $PARAMS_FILE_PATH --capabilities CAPABILITY_IAM --on-failure DELETE
elif [[ $UPDATE_SWITCH == "yes" ]]; then
  echo "updating mode stack"
  aws cloudformation update-stack --stack-name $STACK_NAME --template-body $TEMPLATE_FILE_PATH --parameters $PARAMS_FILE_PATH --capabilities CAPABILITY_IAM
fi